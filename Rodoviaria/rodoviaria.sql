CREATE DATABASE rodoviaria;
USE rodoviaria;

CREATE TABLE passageiro(
    idpassageiro INT AUTO_INCREMENT,
	nome varchar(50),
	senha varchar(10) NOT NULL,
	genero varchar(20) NOT NULL,
	rg varchar(10) NOT NULL,
	cpf varchar(13),
	endereco varchar(60),
	email varchar(20),
	telefone varchar(14),
	PRIMARY KEY (idpassageiro)
);