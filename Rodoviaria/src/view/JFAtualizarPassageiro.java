package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;

public class JFAtualizarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField textNome;
	private JTextField textRG;
	private JTextField textCPF;
	private JTextField textEmail;
	private JTextField textEndereco;
	private JTextField textTelefone;
	private JTextField passwordSenha;
	private JLabel lblAlterar;
	private JLabel lblidf;
	private static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAtualizarPassageiro frame = new JFAtualizarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param id2 
	 */
	public JFAtualizarPassageiro(int id) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 586, 413);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PassageiroDAO fdao = new PassageiroDAO();
		Passageiro f = fdao.read(id);
		
		
		JLabel lblNewLabel;
		lblidf = new JLabel("ID Passageiro:");
		lblidf.setBounds(408, 10, 66, 13);
		contentPane.add(lblidf);
		
		
		JLabel lblID = new JLabel("New label");
		lblID.setBounds(484, 10, 78, 13);
		contentPane.add(lblID);
		JLabel lblNewLabel_1;
		lblAlterar = new JLabel("Alterar passageiro");
		lblAlterar.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		lblAlterar.setBounds(202, 10, 177, 26);
		contentPane.add(lblAlterar);

		
		
		JLabel Nome = new JLabel("NOME");
		Nome.setFont(new Font("Tahoma", Font.PLAIN, 12));
		Nome.setBounds(10, 47, 120, 21);
		contentPane.add(Nome);
		
		textNome = new JTextField();
		textNome.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textNome.setBounds(10, 78, 272, 19);
		contentPane.add(textNome);
		textNome.setColumns(10);
		
		
		JLabel Rg = new JLabel("RG");
		Rg.setFont(new Font("Tahoma", Font.PLAIN, 12));
		Rg.setBounds(334, 52, 45, 13);
		contentPane.add(Rg);
		
		textRG = new JTextField();
		textRG.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textRG.setBounds(334, 78, 202, 19);
		contentPane.add(textRG);
		textRG.setColumns(10);
		
		JLabel Cpf = new JLabel("CPF");
		Cpf.setFont(new Font("Tahoma", Font.PLAIN, 12));
		Cpf.setBounds(334, 107, 45, 13);
		contentPane.add(Cpf);
		
		textCPF = new JTextField();
		textCPF.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textCPF.setBounds(334, 127, 202, 19);
		contentPane.add(textCPF);
		textCPF.setColumns(10);
		
		JLabel Email = new JLabel("EMAIL");
		Email.setFont(new Font("Tahoma", Font.PLAIN, 12));
		Email.setBounds(10, 108, 45, 13);
		contentPane.add(Email);
		
		textEmail = new JTextField();
		textEmail.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textEmail.setBounds(10, 127, 272, 19);
		contentPane.add(textEmail);
		textEmail.setColumns(10);
		
		JLabel Endereco = new JLabel("ENDERECO");
		Endereco.setFont(new Font("Tahoma", Font.PLAIN, 12));
		Endereco.setBounds(10, 173, 85, 13);
		contentPane.add(Endereco);
		
		textEndereco = new JTextField();
		textEndereco.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textEndereco.setBounds(10, 191, 272, 45);
		contentPane.add(textEndereco);
		textEndereco.setColumns(10);
		
		JLabel Telefone = new JLabel("TELEFONE");
		Telefone.setFont(new Font("Tahoma", Font.PLAIN, 12));
		Telefone.setBounds(10, 252, 78, 13);
		contentPane.add(Telefone);
		
		textTelefone = new JTextField();
		textTelefone.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textTelefone.setBounds(10, 278, 272, 23);
		contentPane.add(textTelefone);
		textTelefone.setColumns(10);
		
		JLabel Senha = new JLabel("SENHA DE ACESSO");
		Senha.setForeground(Color.DARK_GRAY);
		Senha.setFont(new Font("Tahoma", Font.BOLD, 12));
		Senha.setBounds(334, 252, 125, 13);
		contentPane.add(Senha);
		
		passwordSenha = new JTextField();
		passwordSenha.setBounds(331, 280, 205, 19);
		contentPane.add(passwordSenha);
		passwordSenha.setColumns(10);
		
		
		
		lblID.setText(String.valueOf(f.getidpassageiro()));
		textEmail.setText(String.valueOf(f.getEmail()));
		textEndereco.setText(String.valueOf(f.getEndereco()));
		textTelefone.setText(String.valueOf(f.getTelefone()));
		textRG.setText(String.valueOf(f.getRg()));
		textCPF.setText(String.valueOf(f.getCpf()));
		passwordSenha.setText(String.valueOf(f.getSenha()));
		
		
		JButton btnAlterar = new JButton("ALTERAR");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro f = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				f.setidpassageiro(Integer.parseInt(lblID.getText()));
				f.setNome(textNome.getText());
				f.setEmail(textEmail.getText());
				f.setEndereco(textEndereco.getText());
				f.setTelefone(textTelefone.getText()); 
				f.setRg(textRG.getText());
				f.setCpf(textCPF.getText());
				f.setSenha(passwordSenha.getText());
			 
			dao.update(f);
			dispose();
		}
		});
		
		
		btnAlterar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnAlterar.setBounds(53, 332, 120, 21);
		contentPane.add(btnAlterar);
		
		JButton btnLimpar = new JButton("LIMPAR");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textNome.setText(null);
				textEmail.setText(null);
				textEndereco.setText(null);
				textTelefone.setText(null);
				textRG.setText(null);
				textCPF.setText(null);
				passwordSenha.setText(null);
			}
		});
		btnLimpar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLimpar.setBounds(231, 332, 95, 21);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("CANCELAR");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnCancelar.setBounds(392, 332, 105, 21);
		contentPane.add(btnCancelar);
	}

	}
