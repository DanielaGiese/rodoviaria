package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		setTitle("SisRodoviaria_tela de login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Sistema Rodoviaria - Bem Vindo!");
		lblNewLabel.setFont(new Font("Impact", Font.PLAIN, 19));
		lblNewLabel.setBounds(79, -8, 263, 36);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Login:");
		lblNewLabel_1.setFont(new Font("Impact", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(10, 38, 138, 28);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Usu\u00E1rio:");
		lblNewLabel_2.setFont(new Font("Impact", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(10, 77, 69, 18);
		contentPane.add(lblNewLabel_2);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(78, 78, 348, 21);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Senha:");
		lblNewLabel_3.setFont(new Font("Impact", Font.PLAIN, 14));
		lblNewLabel_3.setBounds(10, 123, 55, 18);
		contentPane.add(lblNewLabel_3);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(78, 125, 348, 19);
		contentPane.add(txtSenha);
		
		JButton btnAcessar = new JButton("Acessar");
		btnAcessar.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnAcessar.setBounds(34, 186, 85, 21);
		contentPane.add(btnAcessar);
		
		JButton btnLimpar = new JButton("Limpar campos");
		btnLimpar.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnLimpar.setBounds(132, 186, 151, 21);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnCancelar.setBounds(293, 186, 104, 21);
		contentPane.add(btnCancelar);
		
		JButton btnRecuperarSenha = new JButton("Recuperar senha");
		btnRecuperarSenha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRecuperarSenha.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		btnRecuperarSenha.setBounds(10, 233, 162, 19);
		contentPane.add(btnRecuperarSenha);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		btnCadastrar.setBounds(310, 232, 104, 21);
		contentPane.add(btnCadastrar);
	}
}
