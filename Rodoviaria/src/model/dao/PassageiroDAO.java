package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Passageiro;

public class PassageiroDAO {
	public void create(Passageiro f) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("INSERT INTO passageiro (idpassageiro, nome, senha, rg, cpf, endereco, email, telefone) VALUES (?,?,?,?,?,?,?,?)");
			stmt.setInt(1, f.getidpassageiro());
			stmt.setString(2, f.getNome());
			stmt.setString(3, f.getSenha());
			stmt.setString(4, f.getRg());
			stmt.setString(5, f.getCpf());
			stmt.setString(6, f.getEndereco());
			stmt.setString(7, f.getEmail());
			stmt.setString(8, f.getTelefone());

			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public List<Passageiro> read(){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Passageiro> passageiros = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Passageiro f = new Passageiro();
				f.setidpassageiro(rs.getInt("idpassageiro"));
				f.setNome(rs.getString("nome"));
				f.setSenha(rs.getString("senha"));
				f.setRg(rs.getString("rg"));
				f.setCpf(rs.getString("cpf"));
				f.setEndereco(rs.getString("endereco"));
				f.setEmail(rs.getString("email"));
				f.setTelefone(rs.getString("telefone"));

				passageiros.add(f);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informações do BD" + e);
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return passageiros;
			
	}	
	
	public void delete(Passageiro f) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null; 
		
		try {
			stmt = con.prepareStatement("DELETE FROM passageiro WHERE idpassageiro=?");
			stmt.setInt(1, f.getidpassageiro());
			stmt.executeUpdate();
			  
			JOptionPane.showMessageDialog(null, "Passageiro excluido com sucesso");
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exclur: "+ e);
		}finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
		
	}
	
	
	 public Passageiro read(int id) {
		 Connection con = ConnectionFactory.getConnection();
		 PreparedStatement stmt = null;
		 ResultSet rs = null;
		 Passageiro f = new Passageiro();
		 
		 try {
			stmt = con.prepareStatement("SELECT * FROM passageiro WHERE idpassageiro=? 	LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			
			if(rs != null && rs.next()) {
				f.setidpassageiro(rs.getInt("idpassageiro"));
				f.setNome(rs.getString("nome"));
				f.setSenha(rs.getString("senha"));
				f.setRg(rs.getString("rg"));
				f.setCpf(rs.getString("cpf"));
				f.setEndereco(rs.getString("endereco"));
				f.setEmail(rs.getString("email"));
				f.setTelefone(rs.getString("telefone"));
			}
} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
}
		 return f;
		 
	 }
	 
	 public void update(Passageiro f) {
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement stmt = null;
			
			try {
				stmt = con.prepareStatement("UPDATE passageiro SET nome=?, senha=?, rg=?, cpf=?, endereco=?, email=?, telefone=? WHERE idpassageiro=?;");
				stmt.setString(1, f.getNome());
				stmt.setString(2, f.getSenha());
				stmt.setString(3, f.getRg());
				stmt.setString(4, f.getCpf());
				stmt.setString(5, f.getEndereco());
				stmt.setString(6, f.getEmail());
				stmt.setString(7, f.getTelefone());
				stmt.setInt(7, f.getidpassageiro());
				stmt.executeUpdate();
				
				JOptionPane.showMessageDialog(null, "Alterado com sucesso");
			}catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
			} finally {
				ConnectionFactory.closeConnection(con, stmt);
			}
		}
	 
	 
}
	 
